#!/usr/bin/env python3
import os
import sys
from flask import Flask, request, send_from_directory
app = Flask(__name__, static_url_path='')

if len(sys.argv) > 1:
    stl_folder = os.path.join(os.getcwd(), sys.argv[1])
else:
    stl_folder = os.getcwd()

@app.route('/stls/<path:path>')
def stls(path):
    return send_from_directory(stl_folder, path)

@app.route('/')
def index():
    return send_from_directory('static', 'index.html')

@app.route('/<path:path>')
def root(path):
    return send_from_directory('static', path)

def main():
    app.run(host="localhost", port=8888)

if __name__ == '__main__':
    main()
