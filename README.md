# [microscope-stls.openflexure.org](https://microscope-stls.openflexure.org)

> STL file selector for the OpenFlexure Microscope


## Running the STL file selector locally

This section explains how to run the STL selector locally on your most recent build of the Microscope STLs. You will need to ensure that you also generate the JSON file with the selection options when building (shown below).

### Installing the selector locally

Install NodeJS (e.g. on Ubuntu 20.04 `sudo apt install nodejs`) and Python3, then:

```
npm install && npm run build-cli && pip3 install .
```

### Running the selector locally

In the microscope directory

```
./build.py --generate-stl-options-json
stl-selector docs/models
```

Go to [localhost:8888](http://localhost:8888)

This will show you how your local build of the Open Flexure Microscope would display on `microscope-stls.openflexure.org` when released.

![screenshot of the stl-selector browser window](screenshot.png)

## Developing the web app

### Installing
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

Vist http://localhost:8080 to see the dev site.

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize Vue configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
