const lodash = require("lodash");
const { computeStls } = require("./computeStls");

function preComputeData(stlOptions) {
  const {
    stls,
    options: originalOptions,
    docs,
    required,
    presets,
  } = stlOptions;
  const optionNames = {};
  const optionData = {};
  const options = {};
  for (const k in originalOptions) {
    optionNames[k] = lodash.startCase(k);
    const optionDocs = docs.find((o) => o.key === k);
    optionData[k] = optionDocs.default;
    if (originalOptions[k] !== "bool") {
      if (optionDocs.options != null) {
        options[k] = optionDocs.options.map((o) => ({
          text: o.title,
          description: o.description,
          value: o.key,
        }));
      } else {
        options[k] = originalOptions[k].map((value) => ({
          text: String(value).replace(/_/g, " "),
          description: "",
          value,
        }));
      }
    } else {
      options[k] = originalOptions[k];
    }
  }
  const optionOrder = docs
    .filter((o) => !o.advanced)
    .map((o) => o.key)
    .filter((key) => options[key] != null);
  const advancedOptionOrder = docs
    .filter((o) => o.advanced)
    .map((o) => o.key)
    .filter((key) => options[key] != null);
  const descriptions = docs.reduce(
    (descriptions, o) => ({
      ...descriptions,
      [o.key]: o.description,
    }),
    {}
  );

  const defaults = docs.reduce(
    (defaults, o) => ({ ...defaults, [o.key]: o.default }),
    {}
  );
  let { invalid, neverChangingStls, valid } = computeCombinations(
    originalOptions,
    required,
    stls
  );

  if (checkIsInvalid(defaults, invalid)) {
    console.error("Initial defaults are not in a valid configuration");
    const computed = computeStls(stls, defaults);
    required.forEach((r) => {
      if (computed.findIndex((stl) => RegExp(r).test(stl)) === -1) {
        console.error("Failed regex:", r);
        console.error("Computed stls:", computed);
        console.error("defaults:", defaults);
      }
    });
    process.exit(1);
  }

  for (const preset of presets) {
    const presetOptions = { ...defaults, ...preset.parameters };
    const presetKeys = Object.keys(presetOptions).sort();
    const validKeys = Object.keys(options).sort();
    if (!lodash.isEqual(presetKeys, validKeys)) {
      for (const key of presetKeys) {
        if (!validKeys.includes(key)) {
          console.error("preset", preset.key, "includes invalid key:", key);
        }
      }
      for (const key of validKeys) {
        if (!presetKeys.includes(key)) {
          console.error(
            "preset",
            preset.key,
            "doesn't have required key:",
            key
          );
        }
      }
      process.exit(1);
    }
    if (checkIsInvalid(presetOptions, invalid)) {
      console.error("Preset sets invalid options:", preset.key);
      const computed = computeStls(stls, presetOptions);
      required.forEach((r) => {
        if (computed.findIndex((stl) => RegExp(r).test(stl)) === -1) {
          console.error("Failed regex:", r);
        }
      });
      process.exit(1);
    }
  }

  return {
    allStls: stls,
    descriptions,
    options,
    required,
    presets,
    optionNames,
    optionData,
    optionOrder,
    advancedOptionOrder,
    originalOptions,
    neverChanging: Array.from(neverChangingStls),
    invalid,
    valid,
  };
}

function checkIsInvalid(optionData, invalidGrouped) {
  for (const [group, invalids] of invalidGrouped) {
    for (const invalid of invalids) {
      let isInvalid = true;
      for (const k of group) {
        isInvalid = isInvalid && optionData[k] == invalid[k];
      }
      if (isInvalid) {
        return true;
      }
    }
  }
  return false;
}

function computeCombinations(options, required, allStls) {
  const { groups, neverChangingStls } = groupParams(allStls);
  const validGrouped = new Map();
  const invalidGrouped = new Map();
  for (const group of groups) {
    const invalid = [];
    let valid = [];
    const keys = Array.from(group);
    for (const k of keys) {
      if (options[k] == null) {
        console.error(`"${k}" not defined in "options" in stl_options.json`);
      }
    }
    const values = keys
      .map((k) => options[k])
      .map((v) => (v === "bool" ? [true, false] : v));
    const permutations = cartesian(values);
    for (const permutation of permutations) {
      const optionData = {};
      permutation.forEach((v, i) => {
        optionData[keys[i]] = v;
      });
      const stls = computeStls(allStls, optionData);
      const hasRequired = required.reduce((hasRequired, r) => {
        return (
          hasRequired && stls.findIndex((stl) => RegExp(r).test(stl)) !== -1
        );
      }, true);

      if (hasRequired) {
        valid.push(optionData);
      } else {
        invalid.push(optionData);
      }
    }
    validGrouped.set(group, valid);
    invalidGrouped.set(group, invalid);
  }
  return { valid: validGrouped, invalid: invalidGrouped, neverChangingStls };
}

// groups parameters on intersections of affected stls and associated parameters
function groupParams(allStls) {
  let groups = new Map();
  let prevGroups = null;
  // eslint-disable-next-line no-constant-condition
  while (true) {
    for (const stl of allStls) {
      const filename = stl.stl;
      const params = Object.keys(stl.parameters);
      let has = false;
      for (const [k, v] of groups) {
        const hasStl = k.has(filename);
        const hasParam = params.reduce(
          (hasParam, p) => hasParam || v.has(p),
          false
        );
        if (hasStl || hasParam) {
          k.add(filename);
          for (const p of params) {
            v.add(p);
          }
          has = true;
        }
      }
      if (!has) {
        groups.set(new Set([filename]), new Set(params));
      }
    }
    if (lodash.isEqual(groups, prevGroups)) {
      break;
    }
    prevGroups = lodash.cloneDeep(groups);
  }

  const emptySet = new Set();
  const g2 = Array.from(groups.values()).filter(
    (s) => !lodash.isEqual(s, emptySet)
  );
  const neverChangingStls = new Set();
  for (const [stls, params] of groups) {
    if (lodash.isEqual(params, emptySet)) {
      for (const s of stls) {
        neverChangingStls.add(s);
      }
    }
  }
  return {
    groups: lodash.uniqWith(g2, lodash.isEqual),
    neverChangingStls,
  };
}

// the cartesian product, gives us all permutations of an array of arrays of
// possible values, from https://stackoverflow.com/a/15310051
function cartesian(values) {
  let r = [],
    max = values.length - 1;
  function helper(arr, i) {
    for (let j = 0, l = values[i].length; j < l; j++) {
      let a = arr.slice(0); // clone arr
      a.push(values[i][j]);
      if (i == max) {
        r.push(a);
      } else {
        helper(a, i + 1);
      }
    }
  }
  helper([], 0);
  return r;
}

module.exports = { preComputeData };
