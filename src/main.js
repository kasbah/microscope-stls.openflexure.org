import Vue from "vue";
import VueRouter from "vue-router";
import SuiVue from "semantic-ui-vue";
import VueClipboard from "vue-clipboard2";
import StlSelector from "./views/StlSelector.vue";

VueClipboard.config.autoSetContainer = true;
Vue.config.productionTip = false;
Vue.use(VueClipboard);
Vue.use(SuiVue);
Vue.use(VueRouter);

const VUE_APP_IS_CLI = process.env.VUE_APP_IS_CLI === "true";

let redirects;
if (VUE_APP_IS_CLI) {
  redirects = [
    {
      path: "/",
      redirect: "/preview-version",
    },
  ];
} else {
  redirects = [
    {
      path: "/",
      redirect: "/stl-selector-test-version-2",
    },
    {
      path: "/v6.1.4",
      redirect: "/v6.1.5",
    },
  ];
}
console.log({ redirects });

const routes = redirects.concat([
  {
    path: "/:buildVersion",
    name: "stl-selector",
    component: StlSelector,
  },
]);

console.log({ routes });

const router = new VueRouter({ routes });

const App = {
  template: "<router-view />",
};

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
