const { computeStls, computeIsValid } = require("./computeStls");
const lodash = require("lodash");

// computes options that result in invalid "lists of stls" or have no effect on the list
function computeDisabledOptions(allStls, options, optionData, required) {
  const disabledOptions = {};
  const noEffectOptions = {};
  const stls = computeStls(allStls, optionData);
  for (const key in options) {
    disabledOptions[key] = [];
    noEffectOptions[key] = [];
    let possibleValues = options[key];
    if (possibleValues === "bool") {
      possibleValues = [true, false];
    }
    for (const v of possibleValues.filter((v) => v !== optionData[key])) {
      const optionDataV = { ...optionData, [key]: v };
      const isValid = computeIsValid(allStls, optionDataV, required);
      if (!isValid) {
        disabledOptions[key].push(v);
      } else {
        const hasNoEffect = lodash.isEqual(
          stls,
          computeStls(allStls, optionDataV)
        );
        if (hasNoEffect) {
          noEffectOptions[key].push(v);
        }
      }
    }
    if (disabledOptions[key].length === 0) {
      delete disabledOptions[key];
    }
    if (noEffectOptions[key].length === 0) {
      delete noEffectOptions[key];
    }
  }
  return { disabled: disabledOptions, noEffect: noEffectOptions };
}

module.exports = { computeDisabledOptions };
