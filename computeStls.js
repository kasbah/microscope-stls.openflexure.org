const _ = require("lodash");

function computeStls(allStls, optionData) {
  let stls = allStls.map((v) => v.stl);
  for (const v of allStls) {
    for (const param in v.parameters) {
      if (param in optionData) {
        const o = optionData[param];
        const p = v.parameters[param];
        if ((_.isArray(p) && !p.includes(o)) || (!_.isArray(p) && p !== o)) {
          stls = removeOne(stls, v.stl);
          break;
        }
      }
    }
  }
  return _.uniq(stls);
}

function encodeOptions(optionData) {
  return Object.keys(optionData)
    .sort()
    .map((k) => String(optionData[k]))
    .join("|");
}

function removeOne(arr, value) {
  var index = arr.indexOf(value);
  if (index > -1) {
    arr.splice(index, 1);
  }
  return arr;
}

function computeIsValid(allStls, optionData, required) {
  const stls = computeStls(allStls, optionData);
  return computeHasRequired(required, stls);
}

function computeHasRequired(required, stls) {
  return required.reduce((hasRequired, r) => {
    return hasRequired && stls.findIndex((stl) => RegExp(r).test(stl)) !== -1;
  }, true);
}

module.exports = {
  computeStls,
  encodeOptions,
  computeHasRequired,
  computeIsValid,
};
